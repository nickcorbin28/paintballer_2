// Fill out your copyright notice in the Description page of Project Settings.

#include "MyActor.h"


// Creates a default subobject that uses our procedural mesh component plugin
AMyActor::AMyActor()
{
	mesh = CreateDefaultSubobject <UProceduralMeshComponent>(TEXT("GeneratedMesh"));
	RootComponent = mesh;
}

// Called when the game starts or when spawned
void AMyActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// This is called when actor is already in level and map is opened. 
void  AMyActor::PostActorCreated() {
	Super::PostActorCreated();
	CreateSquare();
}  

void  AMyActor::PostLoad() {
	Super::PostLoad();
	CreateSquare();
}

// Adds vertices to a vertex buffer of all vertex/triangle positions to use in a mesh section
void  AMyActor::CreateSquare() {
	TArray <FVector> Vertices;
	TArray <int32> Triangles;
	TArray <FVector> Normals;
	TArray < FLinearColor > Colors;
	Vertices.Add(FVector(0.f, 0.f, 0.f));
	Vertices.Add(FVector(0.f, 100.f, 0.f));
	Vertices.Add(FVector(0.f, 0.f, 100.f));
	Vertices.Add(FVector(0.f, 100.f, 100.f));
	Triangles.Add(0);
	Triangles.Add(1);
	Triangles.Add(2);
	Triangles.Add(3);
	Triangles.Add(2);
	Triangles.Add(1);
	for (int32 i = 0; i < Vertices.Num(); i++) 
	{
		Normals.Add(FVector(0.f, 0.f, 1.f));
		Colors.Add(FLinearColor::Red);
	} // Optional arrays.
	TArray <FVector2D> UV0;
	TArray <FProcMeshTangent> Tangents;
	mesh->CreateMeshSection_LinearColor(0, Vertices, Triangles, Normals, UV0, Colors, Tangents, true);
}

// How MyActor didn't work as intended: Applying materials that are complex, i.e. more than one color, does not show correctly.
// For example, I made a paintball sign-up poster in Photoshop that was intended to be applied to the procedural mesh component square.
// However, the texture simply shows up as black in-engine. To compare, I placed default Plane StaticMeshActor with the material applied.
// I have the option to create a static mesh, but that appears to do nothing to the material. Did I do something wrong?