// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "paintballer_corbinHUD.generated.h"

UCLASS()
class Apaintballer_corbinHUD : public AHUD
{
	GENERATED_BODY()

public:
	Apaintballer_corbinHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

