// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "paintballer_corbinGameMode.h"
#include "paintballer_corbinHUD.h"
#include "paintballer_corbinCharacter.h"
#include "UObject/ConstructorHelpers.h"

Apaintballer_corbinGameMode::Apaintballer_corbinGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = Apaintballer_corbinHUD::StaticClass();
}
