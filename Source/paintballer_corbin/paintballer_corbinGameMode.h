// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "paintballer_corbinGameMode.generated.h"

UCLASS(minimalapi)
class Apaintballer_corbinGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	Apaintballer_corbinGameMode();
};



