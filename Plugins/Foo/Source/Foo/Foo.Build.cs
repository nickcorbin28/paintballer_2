// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
using UnrealBuildTool;
public class Foo : ModuleRules
{
    public Foo(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
        PublicIncludePaths.AddRange(new string[] {
            "Foo/Public" // Add public include paths required here.
        });
        PrivateIncludePaths.AddRange(new string[] {
            "Foo/Private", // Add other private include paths required here.
        });
        PublicDependencyModuleNames.AddRange(new string[] {
            "Core", "RenderCore", "ShaderCore", "RHI", // Add other public dependencies that you statically link with here.
        });
        PrivateDependencyModuleNames.AddRange(new string[] {
            "CoreUObject", "Engine", // Add private dependencies that you statically link with here.
        });
        DynamicallyLoadedModuleNames.AddRange(new string[] { // Add any modules that your module loads dynamically here.
        });
    }
}