// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Corbin_Plugin.h"

#define LOCTEXT_NAMESPACE "FCorbin_PluginModule"

void FCorbin_PluginModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	UE_LOG(LogTemp, Warning, TEXT("Hello World"));
}

void FCorbin_PluginModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FCorbin_PluginModule, Corbin_Plugin)